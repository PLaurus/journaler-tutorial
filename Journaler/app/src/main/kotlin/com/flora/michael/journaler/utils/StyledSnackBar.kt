package com.flora.michael.journaler.utils

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.flora.michael.journaler.R
import com.google.android.material.snackbar.Snackbar

class StyledSnackBar {
    companion object{
        fun make(container: View, message: String, duration: Int) : Snackbar{
            return Snackbar.make(container, message, duration).apply {
                view.run{
                    background = ContextCompat.getDrawable(context, R.drawable.shape_light_grey_border)
                    findViewById<TextView>(R.id.snackbar_text).run{
                        setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        elevation = 2.0f
                        isAllCaps = true
                    }
                }
            }
        }
    }
}