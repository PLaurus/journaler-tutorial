package com.flora.michael.journaler.ui.main.todos

import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.flora.michael.journaler.R
import com.flora.michael.journaler.model.entity.Todo
import com.flora.michael.journaler.ui.abstracted.fragment.ListFragment
import com.flora.michael.journaler.ui.main.MainViewModel

class TodosListFragment : ListFragment() {
    override val logTag: String = TodosListFragment::class.java.simpleName
    override val itemsCount: LiveData<Int>
        get() = viewModel.todosSize

    private val viewModel by lazy{
        ViewModelProviders.of(this)[MainViewModel::class.java]
    }

    private var todosAdapter: TodosAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        todosAdapter = TodosAdapter(this, viewModel)

        todosAdapter?.run{
            viewModel.allTodos.observe(viewLifecycleOwner,
                Observer<List<Todo>> {
                    elements = it
                })
        }

        view.findViewById<RecyclerView>(R.id.items)?.adapter = todosAdapter

        super.onViewCreated(view, savedInstanceState)
    }
}