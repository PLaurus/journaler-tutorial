package com.flora.michael.journaler.ui.main.todos


import android.content.Context
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.flora.michael.journaler.R
import com.flora.michael.journaler.model.entity.Todo
import com.flora.michael.journaler.model.entity.abstracted.DbEntry
import com.flora.michael.journaler.ui.abstracted.adapter.EntryAdapter
import com.flora.michael.journaler.ui.main.MainViewModel
import com.flora.michael.journaler.utils.millisToDays
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class TodosAdapter(private val lifecycleOwner: LifecycleOwner?, private val viewModel: MainViewModel) : EntryAdapter(){
    private val tag = TodosAdapter::class.java.simpleName

    companion object{
        const val DATE_FORMAT_PATTERN = "MM.dd.YYYY"
        const val TIME_FORMAT_PATTERN = "HH : mm"
        const val DATE_TIME_FROMAT = "%s - %s"
    }

    private val dateFormat = SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.ENGLISH)
    private val timeFormat = SimpleDateFormat(TIME_FORMAT_PATTERN, Locale.ENGLISH)

    override var elements: List<DbEntry>? = null
        set(value){
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        val card = holder.card
        val firstDateTextContainer = card.findViewById<TextView>(R.id.date_text_container_1)
        val secondDateTextContainer = card.findViewById<TextView>(R.id.date_text_container_2)
        val dateWordTextContainer = card.findViewById<TextView>(R.id.date_word)

        val element = (elements?.get(position) as Todo)

        (firstDateTextContainer to secondDateTextContainer).setDate(element.fromDate, element.toDate)

        dateWordTextContainer.setDateWord(element.fromDate)

    }

    override val onDeleteItemPressed = { context: Context, position : Int ->
        lifecycleOwner?.lifecycleScope?.launch{
            val result = viewModel.viewModelScope.async {
                viewModel.delete(elements?.get(position) as? Todo ?: throw IllegalArgumentException("Todo expected!"))
            }

            var message = "Failed to delete todo"
            if(result.await() > 0) message = "Deleted todo"

            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }

        Unit
    }

    override val onEditItemPressed = { context: Context, position : Int ->
        //TODO("Open editor activity with passed intent")
    }

    private fun Pair<TextView,TextView>.setDate(fromTimeMillis: Long, toTimeMillis: Long){
        val currentDate = Calendar.getInstance()
        val fromDate = Calendar.getInstance().apply { timeInMillis = fromTimeMillis }
        val toDate =  Calendar.getInstance().apply { timeInMillis = toTimeMillis }

        val fromDateText = dateFormat.format(fromDate.time)
        val fromTimeText = timeFormat.format(fromDate.time)
        val toDateText = dateFormat.format(toDate.time)
        val toTimeText = timeFormat.format(toDate.time)

        if(fromDate == toDate){
            if(fromDate.timeInMillis.millisToDays() == currentDate.timeInMillis.millisToDays()){
                first.text = fromTimeText
            }else{
                first.text = String.format(DATE_TIME_FROMAT, fromDateText, fromTimeText)
            }
            first.visibility = View.VISIBLE
            second.visibility = View.GONE
        }else{
            if(fromDate.timeInMillis.millisToDays() == currentDate.timeInMillis.millisToDays()){
                first.text = fromTimeText
                second.text = String.format(DATE_TIME_FROMAT, toDateText, toTimeText)
            }else{
                first.text = String.format(DATE_TIME_FROMAT, fromDateText, fromTimeText)
                second.text = String.format(DATE_TIME_FROMAT, toDateText, toTimeText)
            }
            first.visibility = View.VISIBLE
            second.visibility = View.VISIBLE
        }
    }

    private fun TextView.setDateWord(timeMillis: Long){
        val currentDate = Calendar.getInstance()
        val todoDate = Calendar.getInstance().apply{ timeInMillis = timeMillis }

        val differenceDays : Int? = if(todoDate.timeInMillis.millisToDays() >= currentDate.timeInMillis.millisToDays()) currentDate.get(Calendar.DAY_OF_MONTH) - todoDate.get(Calendar.DAY_OF_MONTH) else null

        when(differenceDays){
            0 -> text = context.getText(R.string.card_todo_today)
            -1 -> text = context.getText(R.string.card_todo_tomorrow)
            -2 -> text = context.getText(R.string.card_todo_day_after_tomorrow)
            else ->{
                visibility = View.GONE
                return
            }
        }

        visibility = View.VISIBLE
    }

}