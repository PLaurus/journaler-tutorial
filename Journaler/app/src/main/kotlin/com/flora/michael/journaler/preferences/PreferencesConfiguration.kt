package com.flora.michael.journaler.preferences

import android.content.Context

data class PreferencesConfiguration(val key: String, val mode: Int = Context.MODE_PRIVATE)