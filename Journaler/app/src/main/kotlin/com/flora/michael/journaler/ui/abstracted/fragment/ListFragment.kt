package com.flora.michael.journaler.ui.abstracted.fragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.flora.michael.journaler.R
import kotlinx.android.synthetic.main.fragment_items.*


abstract class ListFragment : BaseFragment() {
    override val logTag: String = "Cards list fragment"
    override val layoutResource = R.layout.fragment_items

    abstract val itemsCount : LiveData<Int>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        Log.v(logTag,"[ ON CREATE VIEW ]")
        return super.onCreateView(inflater, container, savedInstanceState)?.apply{
            findViewById<RecyclerView>(R.id.items)?.run {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(context)//linearLayoutManager
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        itemsCount.observe(viewLifecycleOwner,
            Observer<Int>{
                showHideEmptyText(it)
            })
    }

    private fun showHideEmptyText(count: Int){
        if(count <= 0){
            no_entries_text.visibility = View.VISIBLE
        }else{
            no_entries_text.visibility = View.GONE
        }
    }
}