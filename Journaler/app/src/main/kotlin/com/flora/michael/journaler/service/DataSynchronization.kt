package com.flora.michael.journaler.service

interface DataSynchronization {
    fun synchronize()
}