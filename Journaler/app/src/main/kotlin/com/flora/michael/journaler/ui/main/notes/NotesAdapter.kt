package com.flora.michael.journaler.ui.main.notes

import android.content.Context
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.flora.michael.journaler.R
import com.flora.michael.journaler.model.entity.Note
import com.flora.michael.journaler.model.entity.abstracted.DbEntry
import com.flora.michael.journaler.ui.abstracted.adapter.EntryAdapter
import com.flora.michael.journaler.ui.main.MainViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class NotesAdapter(private val lifecycleOwner: LifecycleOwner?, private val viewModel: MainViewModel) : EntryAdapter(){
    override var elements: List<DbEntry>? = null
        set(value){
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        with(holder.card) {
            findViewById<TextView>(R.id.date_text_container_1).visibility = View.GONE
            findViewById<TextView>(R.id.date_text_container_2).visibility = View.GONE
        }
    }

    override val onDeleteItemPressed = { context: Context, position : Int ->
        lifecycleOwner?.lifecycleScope?.launch{
            val result = viewModel.viewModelScope.async{
                viewModel.delete(elements?.get(position) as? Note ?: throw IllegalArgumentException("Note expected!"))
            }

            var message = "Failed to delete note"
            if(result.await() > 0) message = "Deleted note"

            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }


        Unit
    }

    override val onEditItemPressed = { context: Context, position : Int ->
        //TODO("Open editor activity with passed intent")
    }
}