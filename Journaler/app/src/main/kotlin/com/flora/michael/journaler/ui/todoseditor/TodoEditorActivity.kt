package com.flora.michael.journaler.ui.todoseditor

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.flora.michael.journaler.R
import com.flora.michael.journaler.ui.abstracted.activity.ItemActivity
import com.flora.michael.journaler.utils.StyledSnackBar
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_todo.*
import kotlinx.android.synthetic.main.save_cancel_buttons.*
import java.util.*


class TodoEditorActivity : ItemActivity() {
    override val tag = "Todo activity"
    override fun getLayout() = R.layout.activity_todo

    companion object{
        private const val DATE_FORMAT_PATTERN = "%02d.%02d.%04d"
        private const val TIME_FORMAT_PATTERN = "%02d : %02d"
    }

    private val viewModel by lazy{
        ViewModelProviders.of(this)[TodoEditorViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.initialize()

        viewModel.error.observe(this, Observer{})

        todo_title.addTextChangedListener { editable ->
            viewModel.title.value = editable?.toString() ?: ""
        }

        todo_content.addTextChangedListener { editable ->
            viewModel.content.value = editable?.toString() ?: ""
        }

        initializeDatePickers()

        save_button.initializeSaveButton()
    }

    private fun initializeDatePickers(){
        pick_from_date.setDatePicker(viewToShow = pick_from_time){ date ->
            viewModel.setDateFrom(date[Calendar.YEAR], date[Calendar.MONTH], date[Calendar.DAY_OF_MONTH])
        }

        pick_from_time.setTimePicker(viewToShow = pick_to_date){time ->
            viewModel.setTimeFrom(time[Calendar.HOUR_OF_DAY], time[Calendar.MINUTE])
        }

        pick_to_date.setDatePicker(viewToShow = pick_to_time){ date ->
            viewModel.setDateTo(date[Calendar.YEAR], date[Calendar.MONTH], date[Calendar.DAY_OF_MONTH])
        }

        pick_to_time.setTimePicker{ time ->
            viewModel.setTimeTo(time[Calendar.HOUR_OF_DAY], time[Calendar.MINUTE])
        }
    }

    private fun MaterialButton.setDatePicker(viewToShow: View? = null, onDateSet : ((date: Calendar) -> Unit)? = null) {
        setOnClickListener{
            val currentDateTime = Calendar.getInstance()

            DatePickerDialog(context,
                DatePickerDialog.OnDateSetListener { _, year, month, day ->
                    this@setDatePicker.text = String.format(Locale.ENGLISH, DATE_FORMAT_PATTERN, day, month + 1, year)
                    currentDateTime.set(year, month, day, 0, 0, 0)
                    onDateSet?.invoke(currentDateTime)
                    viewToShow?.let{
                        (layoutParams as LinearLayout.LayoutParams).weight = 0.5f
                        viewToShow.visibility = View.VISIBLE
                    }
                },
                currentDateTime.get(Calendar.YEAR),
                currentDateTime.get(Calendar.MONTH),
                currentDateTime.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    private fun MaterialButton.setTimePicker(viewToShow: View? = null, onTimeSet : ((time: Calendar) -> Unit)? = null) {
        setOnClickListener{
            val currentDateTime = Calendar.getInstance()

            TimePickerDialog(context,
                TimePickerDialog.OnTimeSetListener { _, hours, minutes ->
                    this@setTimePicker.text = String.format(Locale.ENGLISH, TIME_FORMAT_PATTERN, hours, minutes)

                    currentDateTime.run{
                        set(Calendar.HOUR_OF_DAY, hours)
                        set(Calendar.MINUTE, minutes)
                    }

                    onTimeSet?.invoke(currentDateTime)

                    viewToShow?.let{
                        (layoutParams as LinearLayout.LayoutParams).weight = 0.5f
                        viewToShow.visibility = View.VISIBLE
                    }
                },
                currentDateTime.get(Calendar.HOUR_OF_DAY),
                currentDateTime.get(Calendar.MINUTE),
                true).show()
        }
    }

    private fun MaterialButton.initializeSaveButton(){
        setOnClickListener (object : View.OnClickListener{
            override fun onClick(view: View?) {

                val message = when(viewModel.error.value){
                    TodoEditorViewModel.Error.EMPTY_TODO_TITLE -> "Todo title must not be empty!"
                    TodoEditorViewModel.Error.FROM_DATE_NOT_SET -> "Start date must be set!"
                    TodoEditorViewModel.Error.TO_DATE_IS_LESS_THEN_FROM_DATE -> "Finish date must be more then start date!"
                    else -> null
                }

                message?.let{
                    StyledSnackBar.make(this@TodoEditorActivity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show()
                    return
                }

                viewModel.saveNewTodo()

                finish()
            }
        })
    }
}