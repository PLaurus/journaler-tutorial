package com.flora.michael.journaler.model.entity

import android.location.Location
import android.location.LocationManager
import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import com.flora.michael.journaler.model.entity.abstracted.DbEntry

@Entity(tableName = "notes")
class Note(title:String, message:String, location: Location) : DbEntry(title,message,location), Parcelable{

    constructor(parcel: Parcel?)
            : this(parcel?.readString() ?: "undefined",
        parcel?.readString() ?: "undefined",
        parcel?.readParcelable(Location::class.java.classLoader) ?: Location(LocationManager.GPS_PROVIDER)){
            id = parcel?.readLong() ?: 0L
    }

    override fun writeToParcel(parcel: Parcel?, flags: Int) {
        parcel?.let{
            with(parcel){
                writeString(title)
                writeString(message)
                writeParcelable(location, 0)
                writeLong(id)
            }
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Note> {
        override fun createFromParcel(parcel: Parcel?): Note {
            return Note(parcel)
        }

        override fun newArray(size: Int): Array<Note> {
            return emptyArray()
        }
    }
}