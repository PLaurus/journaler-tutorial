package com.flora.michael.journaler.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.flora.michael.journaler.api.JournalerBackendService
import com.flora.michael.journaler.execution.TaskExecutor

class MainService : Service(), DataSynchronization{
    private val tag = "MainService"
    private var binder = getServiceBinder()
    private var executor = TaskExecutor.getInstance(1)

    override fun onCreate() {
        super.onCreate()
        Log.v(tag, "[ ON CREATE ]")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.v(tag, "[ ON START COMMAND ")
        synchronize()
        return Service.START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.v(tag, "[ ON BIND ]")
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        val result = super.onUnbind(intent)
        Log.v(tag, "[ ON UNBIND ]")
        return result
    }

    override fun onDestroy() {
        synchronize()
        super.onDestroy()
        Log.v(tag, "[ ON DESTROY ]")
    }

    override fun onLowMemory() {
        super.onLowMemory()
        Log.w(tag, "[ ON LOW MEMORY ]")
    }

    override fun synchronize() {
//        executor.execute {
//            Log.i(tag, "Synchronizing data [ START ]")
//
//            var headers = BackendServiceHeaderMap.obtain()
//            val service = JournalerBackendService.obtain()
//            val credentials = UserLoginRequest("username", "password")
//            val tokenResponse = service.login(headers, credentials).execute()
//
//            if(tokenResponse.isSuccessful){
//                val token = tokenResponse.body()
//                token?.let{
//                    TokenManager.currentToken = token
//                    headers = BackendServiceHeaderMap.obtain(true)
//                    fetchNotes(service, headers)
//                    fetchTodos(service, headers)
//                }
//            }
//            Log.i(tag, "Synchronizing data [ END ]")
//        }
    }

    private fun getServiceBinder(): MainServiceBinder = MainServiceBinder()

    private fun fetchNotes(service: JournalerBackendService, headers: Map<String, String>){
//        service.getNotes(headers).enqueue(object : Callback<List<Note>>{
//            override fun onResponse(call: Call<List<Note>>, response: Response<List<Note>>) {
//                if(response.isSuccessful){
//                    val notes = response.body()
//                    notes?.let{
//                        Db.NOTE.insert(notes)
//                    }
//                }
//            }
//
//            override fun onFailure(call: Call<List<Note>>, t: Throwable) {
//                Log.e(tag, "We couldn't fetch notes.")
//            }
//        })
    }

    private fun fetchTodos(service: JournalerBackendService, headers: Map<String, String>){
//        service.getTodods(headers).enqueue(object : Callback<List<Todo>>{
//            override fun onResponse(call: Call<List<Todo>>, response: Response<List<Todo>>) {
//                if(response.isSuccessful){
//                    val notes = response.body()
//                    notes?.let{
//                        Db.TODO.insert(notes)
//                    }
//                }
//            }
//
//            override fun onFailure(call: Call<List<Todo>>, t: Throwable) {
//                Log.e(tag, "We couldn't fetch todos.")
//            }
//        })
    }

    inner class MainServiceBinder : Binder(){
        fun getService(): MainService = this@MainService
    }
}