package com.flora.michael.journaler.model

import android.location.Location
import androidx.room.TypeConverter
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson

class Converters {

    companion object{
        private val gson = Gson()
    }

    @TypeConverter
    fun fromLocationToString(location: Location) : String = gson.toJson(location)

    @TypeConverter
    fun fromStringToLocation(locationJson: String) : Location = gson.fromJson(locationJson)
}