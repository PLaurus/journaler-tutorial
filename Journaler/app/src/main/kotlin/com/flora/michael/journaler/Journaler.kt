package com.flora.michael.journaler

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.util.Log
import androidx.room.Room
import com.flora.michael.journaler.model.AppDatabase
import com.flora.michael.journaler.receiver.NetworkReceiver
import com.flora.michael.journaler.service.MainService

class Journaler : Application() {
    companion object {
        val tag = "Journaler"
        lateinit var instance : Journaler
            private set

        lateinit var database: AppDatabase

        val context : Context
            get() = instance.applicationContext
    }

    val networkReceiver = NetworkReceiver()

    override fun onCreate() {
        super.onCreate()
        instance = this

        database = Room.databaseBuilder(this, AppDatabase::class.java, "journaler").build()

        Log.v(tag, "[ ON CREATE ]")
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkReceiver, filter)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        Log.w(tag, "[ ON LOW MEMORY ]")
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Log.d(tag, "[ ON TRIM MEMORY ] : $level")
    }

    private fun stopService(){
        val serviceIntent = Intent(this, MainService::class.java)
        stopService(serviceIntent)
    }

}