package com.flora.michael.journaler.ui.abstracted.activity

import android.app.Activity
import android.os.Bundle
import android.util.Log
import com.flora.michael.journaler.R
import com.flora.michael.journaler.model.MODE
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.save_cancel_buttons.*

abstract class ItemActivity : BaseActivity() {
    protected var mode = MODE.VIEW
    protected var success = Activity.RESULT_CANCELED
    override fun getActivityTitle(): Int = R.string.app_name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val data = intent.extras

        data?.let {
            val modeToSet = it.getInt(MODE.EXTRAS_KEY, MODE.VIEW.mode)
            mode = MODE.getByValue(modeToSet)
        }

        cancel_button.initializeCancelButton()

        Log.v(tag, "Mode [ $mode ]")
    }

    override fun onDestroy() {
        super.onDestroy()
        setResult(success)
    }

    private fun MaterialButton.initializeCancelButton(){
        setOnClickListener {
            finish()
        }
    }
}