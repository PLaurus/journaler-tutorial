package com.flora.michael.journaler.ui.abstracted.adapter

import android.view.View
import android.widget.TextView
import androidx.annotation.CallSuper
import com.flora.michael.journaler.R
import com.flora.michael.journaler.model.entity.abstracted.DbEntry
import java.util.*

abstract class EntryAdapter : CardsRecyclerAdapter() {
    private val tag = EntryAdapter::class.java.simpleName

    companion object{
        private const val LOCATION_FORMAT = "Latitude: %f; Longitude: %f"
    }

    @CallSuper
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        with(holder.card) {
            val title = findViewById<TextView>(R.id.title)
            val location = findViewById<TextView>(R.id.location)
            val information = findViewById<TextView>(R.id.information)

            elements?.get(position)?.let { entry ->
                title.text = entry.title

                information.run {
                    visibility = if (entry.message.isNotBlank()) {
                        text = entry.message
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }

                location.text = String.format(
                    Locale.ENGLISH,
                    LOCATION_FORMAT,
                    entry.location.latitude,
                    entry.location.longitude
                )

            }
        }
    }

    override fun getItemCount(): Int = elements?.size ?: 0

    abstract var elements : List<DbEntry>?
}
