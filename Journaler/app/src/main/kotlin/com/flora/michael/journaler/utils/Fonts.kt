package com.flora.michael.journaler.utils

import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.view.children
import com.flora.michael.journaler.R

private const val LOG_TAG = "Fonts utility"

private object CustomFonts {
    private val customFonts: MutableMap<String, Typeface> = mutableMapOf()

    private const val EXO_REGULAR_PATH = "fonts/exo_2_regular.ttf"
    private const val EXO_BOLD_PATH = "fonts/exo_2_bold.ttf"

    fun getExoRegularTypeface(context: Context): Typeface = getFontTypeface(EXO_REGULAR_PATH, context)
    fun getExoBoldTypeface(context: Context): Typeface = getFontTypeface(EXO_BOLD_PATH, context, Typeface.DEFAULT_BOLD)

    private fun getFontTypeface(
        fontPath: String,
        context: Context,
        defaultValue: Typeface = Typeface.DEFAULT
    ): Typeface {
        val fontName = fontPath.substringAfterLast('/')

        if (customFonts[fontName] == null) {
            try {
                customFonts[fontName] = Typeface.createFromAsset(context.assets, fontPath) ?: defaultValue
                Log.v(LOG_TAG, "Initialized $fontName font.")
            } catch (e: Exception) {
                customFonts[fontName] = defaultValue
                Log.e(LOG_TAG, "Failed to initialize $fontName font. [ ${e.localizedMessage} ]")
            }
        }
        return customFonts[fontName]!!
    }

}

fun View.applyCustomFonts() {
    val viewTag = this.tag as? String ?: ""
    when (this) {
        is ViewGroup -> {
            children.forEach {
                it.applyCustomFonts()
            }
        }
        is TextView -> {
            typeface = getTypefaceByTag(viewTag, context)
        }
        is EditText -> {
            typeface = getTypefaceByTag(viewTag, context)
        }
        is Button -> {
            typeface = getTypefaceByTag(viewTag, context)
        }
    }
}

private fun getTypefaceByTag(tag: String, context: Context): Typeface =
    when (tag) {
        context.getString(R.string.tag_font_bold) -> CustomFonts.getExoBoldTypeface(context)
        else -> CustomFonts.getExoRegularTypeface(context)
    }

