package com.flora.michael.journaler.ui.abstracted.adapter

import android.content.Context
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import com.flora.michael.journaler.R
import com.google.android.material.card.MaterialCardView

abstract class CardsRecyclerAdapter : RecyclerView.Adapter<CardsRecyclerAdapter.ViewHolder>() {
    private val tag = "Cards recycler adapter"
    private var attachedItemDecoration: RecyclerView.ItemDecoration? = null

    @CallSuper
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.card){
            setRippleEffect()
            initializeCardClick(position)
        }
    }

    @CallSuper
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.adapter_entry_card,
                parent,
                false
            ) as MaterialCardView
        )

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        attachedItemDecoration = getSpacingItemDecoration(recyclerView.context.resources.getDimension(R.dimen.card_margin).toInt())
        attachedItemDecoration?.let{
            recyclerView.addItemDecoration(it)
        }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        attachedItemDecoration?.let{
            recyclerView.removeItemDecoration(it)
        }
    }

    private fun getSpacingItemDecoration(spacing: Int): RecyclerView.ItemDecoration =
        object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect.left = spacing
                outRect.right = spacing
                outRect.bottom = spacing

                if(parent.getChildAdapterPosition(view) == 0) outRect.top = spacing
            }
        }

    private fun MaterialCardView.initializeCardClick(position: Int){
        setOnLongClickListener { view ->
            view.showPopupMenu(position)
            true
        }
    }

    private fun View.showPopupMenu(position: Int){
        val popupMenu = PopupMenu(context, this)
        popupMenu.inflate(R.menu.menu_popup_entry)
        popupMenu.setForceShowIcon(true)

        popupMenu.setOnMenuItemClickListener { item ->
            when(item.itemId){
                R.id.edit_entry_button -> {
                    onEditItemPressed.invoke(context, position)
                }
                R.id.delete_entry_button -> {
                    onDeleteItemPressed.invoke(context, position)
                }
            }

            true
        }

        popupMenu.show()
    }

    private fun MaterialCardView.setRippleEffect(){
        val attrs = intArrayOf(R.attr.selectableItemBackground)
        val typedArray = context.obtainStyledAttributes(attrs)
        val selectableItemBackground = typedArray.getResourceId(0, 0)
        typedArray.recycle()

        isClickable = true
        isFocusable = true
        foreground = context.getDrawable(selectableItemBackground)
    }

    abstract val onDeleteItemPressed : (context: Context, position: Int) -> Unit
    abstract val onEditItemPressed : (context: Context, position: Int) -> Unit

    class ViewHolder(val card: MaterialCardView) : RecyclerView.ViewHolder(card)
}