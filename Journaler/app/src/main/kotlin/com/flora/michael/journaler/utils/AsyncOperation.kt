package com.flora.michael.journaler.utils

import android.os.AsyncTask

private class AsyncOperation<T> : AsyncTask<() -> T, Unit, List<T>>(){
    override fun doInBackground(vararg delegates: (() -> T)): List<T> {
        val results = mutableListOf<T>()
        for(delegate in delegates){
            results.add(delegate.invoke())
        }

        return results
    }
}

fun <T> asyncExecute(block: () -> T){
    AsyncOperation<T>().execute(block)
}