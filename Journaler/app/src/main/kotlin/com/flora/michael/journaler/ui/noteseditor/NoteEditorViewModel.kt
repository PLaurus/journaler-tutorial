package com.flora.michael.journaler.ui.noteseditor

import android.location.Location
import androidx.lifecycle.*
import com.flora.michael.journaler.Journaler
import com.flora.michael.journaler.model.entity.Note
import com.flora.michael.journaler.model.repository.NotesRepository
import kotlinx.coroutines.launch

class NoteEditorViewModel : ViewModel() {
    private val tag: String = this::class.java.simpleName
    private val notesRepository = NotesRepository(Journaler.database.noteDao)

    var title = MutableLiveData<String>().apply{ value = "" }
    var content = MutableLiveData<String>().apply { value = "" }
    var location = Location("")

    val error : LiveData<Error?> = MediatorLiveData<Error?>().apply {
        val onChanged = Observer<Any?> {
            val title = title.value
            value = when{
                title?.isBlank() == true -> Error.EMPTY_NOTE_TITLE
                else -> null
            }
        }

        addSource(title, onChanged)
    }

    fun initialize(){
        title.value = ""
        content.value = ""
        location = Location("")
    }

    fun saveNewNote() : Boolean{
        if(error.value != null)
            return false

        val note = Note(title.value ?: "", content.value ?: "", location)

        viewModelScope.launch {
            notesRepository.insert(note)
        }

        return true
    }

    enum class Error{
        EMPTY_NOTE_TITLE
    }
}