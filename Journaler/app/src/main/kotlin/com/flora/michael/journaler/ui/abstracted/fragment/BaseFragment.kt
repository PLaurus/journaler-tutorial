package com.flora.michael.journaler.ui.abstracted.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment(){
    protected abstract val logTag: String
    protected abstract val layoutResource: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(layoutResource, container, false)

    override fun onStart() {
        super.onStart()
        Log.v(logTag, "[ ON START ]")
    }

}