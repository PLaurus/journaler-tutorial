package com.flora.michael.journaler.receiver

import android.content.*
import android.net.ConnectivityManager
import android.os.IBinder
import android.util.Log
import com.flora.michael.journaler.service.MainService

class NetworkReceiver : BroadcastReceiver(){
    private val tag = "Network receiver"
    private var service: MainService? = null
    private val serviceConnection = object : ServiceConnection{
        override fun onServiceConnected(p0: ComponentName?, binder: IBinder?) {
            if(binder is MainService.MainServiceBinder){
                service = binder.getService()
                service?.synchronize()
            }
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            service = null
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let{
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = connectivityManager.activeNetworkInfo
            val isConnected = activeNetwork?.isConnectedOrConnecting ?: false
            if(isConnected){
                Log.v(tag, "Connectivity [ AVAILABLE ]")

                if(service == null){
                    val serviceIntent = Intent(context, MainService::class.java)
                    context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
                }else{
                    service?.synchronize()
                }
            }else{
                Log.w(tag, "Connectivity [ UNAVAILABLE ]")
                context.unbindService(serviceConnection)
            }
        }
    }
}