package com.flora.michael.journaler.provider
//
//import android.content.*
//import android.database.Cursor
//import android.database.sqlite.SQLiteDatabase
//import android.database.sqlite.SQLiteQueryBuilder
//import android.net.Uri
//import android.text.TextUtils
//import com.flora.michael.journaler.database.Db
//import com.flora.michael.journaler.database.DbHelper
//import java.sql.SQLException
//
//class JournalerProvider : ContentProvider(){
//
//    companion object{
//        private const val dataTypeNote = "note"
//        private const val dataTypeNotes = "notes"
//        private const val dataTypeTodo = "todo"
//        private const val dataTypeTodos = "todos"
//        private const val NOTE_ALL = 1
//        private const val NOTE_ITEM = 2
//        private const val TODO_ALL = 3
//        private const val TODO_ITEM = 4
//
//        private val matcher = UriMatcher(UriMatcher.NO_MATCH)
//
//        const val AUTHORITY = "com.flora.michael.journaler.provider"
//        const val URL_NOTE = "content://$AUTHORITY/$dataTypeNote"
//        const val URL_TODO = "content://$AUTHORITY/$dataTypeTodo"
//        const val URL_NOTES = "content://$AUTHORITY/$dataTypeNotes"
//        const val URL_TODOS = "content://$AUTHORITY/$dataTypeTodos"
//    }
//
//    private val db: SQLiteDatabase by lazy{
//        DbHelper(Db.name,Db.version).writableDatabase
//    }
//
//    init{
//        matcher.addURI(AUTHORITY, dataTypeNotes, NOTE_ALL)
//        matcher.addURI(AUTHORITY, "$dataTypeNote/#", NOTE_ITEM)
//        matcher.addURI(AUTHORITY, dataTypeTodos, TODO_ALL)
//        matcher.addURI(AUTHORITY, "$dataTypeTodo/#", TODO_ITEM)
//    }
//
//    override fun onCreate(): Boolean = true
//
//    override fun insert(uri: Uri, values: ContentValues?): Uri? {
//        values?.let{
//            db.beginTransaction()
//            val (url, tableName) = getParameters(uri)
//
//            if(!TextUtils.isEmpty(tableName)){
//                val inserted = db.insert(tableName, null, values)
//
//                if(inserted > 0){
//                    db.setTransactionSuccessful()
//                }
//
//                db.endTransaction()
//
//                if(inserted > 0){
//                    val resultUrl = ContentUris.withAppendedId(Uri.parse(url), inserted)
//                    context?.contentResolver?.notifyChange(resultUrl, null)
//                    return resultUrl
//                }
//            }
//            else{
//                throw SQLException("Insert failed, no table for uri: $uri")
//            }
//        }
//
//        throw SQLException("Insert failed: $uri")
//
//    }
//
//    override fun update(uri: Uri, values: ContentValues?, where: String?, whereArgs: Array<out String>?): Int {
//        values?.let{
//            db.beginTransaction()
//            val ( _ , tableName) = getParameters(uri)
//
//            if(!TextUtils.isEmpty(tableName)){
//                val updated = db.update(tableName, values, where, whereArgs)
//
//                if(updated > 0){
//                    db.setTransactionSuccessful()
//                }
//
//                db.endTransaction()
//
//                if(updated > 0){
//                    context?.contentResolver?.notifyChange(uri, null)
//                    return updated
//                }
//            }
//            else{
//                throw SQLException("Update failed, no table for uri: $uri")
//            }
//        }
//
//        throw SQLException("Update failed: $uri")
//    }
//
//    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
//        db.beginTransaction()
//        val ( _ , tableName) = getParameters(uri)
//
//        if(!TextUtils.isEmpty(tableName)){
//            val deletedCount = db.delete(tableName, selection, selectionArgs)
//
//            if(deletedCount > 0){
//                db.setTransactionSuccessful()
//            }
//
//            db.endTransaction()
//
//            if(deletedCount > 0){
//                context?.contentResolver?.notifyChange(uri, null)
//                return deletedCount
//            }
//        }
//        else{
//            throw SQLException("Delete failed, no table for uri: $uri")
//        }
//
//        throw SQLException("Delete failed: $uri")
//    }
//
//    override fun query(uri: Uri, projection: Array<out String>?, selection: String?, selectionArgs: Array<out String>?, sortOrder: String?): Cursor? {
//        val sqliteQueryBuilder = SQLiteQueryBuilder()
//        val ( _, tableName) = getParameters(uri)
//
//        sqliteQueryBuilder.tables = tableName
//        sqliteQueryBuilder.projectionMap = mutableMapOf<String, String>()
//
//        val cursor = sqliteQueryBuilder.query(db, projection, selection, selectionArgs, null, null, null)
//        cursor.setNotificationUri(context?.contentResolver, uri)
//
//        return cursor
//    }
//
//    override fun getType(uri: Uri): String? =
//        when(matcher.match(uri)){
//            NOTE_ALL -> {
//                "${ContentResolver.CURSOR_DIR_BASE_TYPE}/vnd.com.flora.michael.journaler.note.items"
//            }
//            NOTE_ITEM -> {
//                "${ContentResolver.CURSOR_ITEM_BASE_TYPE}/vnd.com.flora.michael.journaler.note.item"
//            }
//            TODO_ALL -> {
//                "${ContentResolver.CURSOR_DIR_BASE_TYPE}/vnd.com.flora.michael.journaler.todo.items"
//            }
//            TODO_ITEM -> {
//                "${ContentResolver.CURSOR_ITEM_BASE_TYPE}/vnd.com.flora.michael.journaler.todo.item"
//            }
//            else -> throw IllegalArgumentException("Unsupported Uri [ $uri ]")
//        }
//
//    private fun getParameters(uri: Uri): Pair<String, String> {
//        if(uri.toString().startsWith(URL_NOTE)){
//            return Pair(URL_NOTE, DbHelper.TABLE_NOTES)
//        }
//        if(uri.toString().startsWith(URL_NOTES)){
//            return Pair(URL_NOTES, DbHelper.TABLE_NOTES)
//        }
//        if(uri.toString().startsWith(URL_TODO)){
//            return Pair(URL_TODO, DbHelper.TABLE_TODOS)
//        }
//        if(uri.toString().startsWith(URL_TODOS)){
//            return Pair(URL_TODOS, DbHelper.TABLE_TODOS)
//        }
//        return Pair("", "")
//    }
//}