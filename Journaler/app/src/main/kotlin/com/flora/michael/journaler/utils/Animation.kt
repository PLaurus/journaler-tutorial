package com.flora.michael.journaler.utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.animation.AccelerateInterpolator
import android.view.animation.BounceInterpolator
import com.google.android.material.floatingactionbutton.FloatingActionButton

fun FloatingActionButton.bounceAnimation(expand: Boolean = true){
    val animation1 = ObjectAnimator.ofFloat(this, "scaleX", if(expand) 1.5f else 1.0f).apply{
        duration = 2000
        interpolator = BounceInterpolator()
    }

    val animation2 = ObjectAnimator.ofFloat(this, "scaleY", if(expand) 1.5f else 1.0f).apply{
        duration = 2000
        interpolator = BounceInterpolator()
    }

    val animation3 = ObjectAnimator.ofFloat(this, "alpha", if(expand) 0.3f else 1.0f).apply{
        duration = 500
        interpolator = AccelerateInterpolator()
    }

    val set = AnimatorSet()
    with(set){
        play(animation1).with(animation2).before(animation3)
        start()
    }
}