package com.flora.michael.journaler.database

//import android.database.sqlite.SQLiteDatabase
//import android.database.sqlite.SQLiteOpenHelper
//import android.util.Log
//import com.flora.michael.journaler.Journaler
//
//class DbHelper(val dbName: String, val version: Int) : SQLiteOpenHelper(Journaler.context, dbName, null, version){
//    companion object{
//        const val ID: String = "_id"
//        const val TABLE_TODOS = "todos"
//        const val TABLE_NOTES = "notes"
//        const val COLUMN_TITLE = "title"
//        const val COLUMN_MESSAGE = "message"
//        const val COLUMN_FROM_TIME = "from_time"
//        const val COLUMN_TO_TIME = "to_time"
//        const val COLUMN_LOCATION = "location"
//    }
//
//    private val tag ="DbHelper"
//
//    private val createTableNotes = """
//        CREATE TABLE if not exists $TABLE_NOTES
//        (
//            $ID integer PRIMARY KEY autoincrement,
//            $COLUMN_TITLE text,
//            $COLUMN_MESSAGE text,
//            $COLUMN_LOCATION text
//        )
//    """.trimIndent()
//
//    private val createTableTodos = """
//        CREATE TABLE if not exists $TABLE_TODOS
//        (
//            $ID integer PRIMARY KEY autoincrement,
//            $COLUMN_TITLE text,
//            $COLUMN_MESSAGE text,
//            $COLUMN_FROM_TIME integer,
//            $COLUMN_TO_TIME integer,
//            $COLUMN_LOCATION text
//        )
//    """.trimIndent()
//
//    override fun onCreate(db: SQLiteDatabase?) {
//        Log.d(tag, "Database [ CREATING ]")
//        db?.execSQL(createTableNotes)
//        db?.execSQL(createTableTodos)
//        Log.d(tag, "Database [ CREATED ]")
//    }
//
//    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) = Unit
//}