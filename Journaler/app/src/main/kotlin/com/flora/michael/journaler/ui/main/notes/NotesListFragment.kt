package com.flora.michael.journaler.ui.main.notes

import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.flora.michael.journaler.R
import com.flora.michael.journaler.model.entity.Note
import com.flora.michael.journaler.ui.abstracted.fragment.ListFragment
import com.flora.michael.journaler.ui.main.MainViewModel

class NotesListFragment : ListFragment() {
    override val logTag: String = NotesListFragment::class.java.simpleName
    override val itemsCount: LiveData<Int>
        get() = viewModel.notesSize

    private val viewModel by lazy{
        ViewModelProviders.of(this)[MainViewModel::class.java]
    }

    private var notesAdapter: NotesAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        notesAdapter = NotesAdapter(this, viewModel)

        notesAdapter?.run{
            viewModel.allNotes.observe(viewLifecycleOwner,
                Observer<List<Note>> {
                    elements = it
                })
        }

        view.findViewById<RecyclerView>(R.id.items)?.adapter = notesAdapter

        super.onViewCreated(view, savedInstanceState)
    }
}