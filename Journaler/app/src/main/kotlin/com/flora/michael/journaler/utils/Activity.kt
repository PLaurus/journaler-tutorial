package com.flora.michael.journaler.utils

import android.app.Activity
import android.view.animation.Animation
import android.view.animation.AnimationUtils

fun Activity.getAnimation(animationResId: Int): Animation = AnimationUtils.loadAnimation(this, animationResId)

