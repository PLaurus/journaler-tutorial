package com.flora.michael.journaler.api

import com.flora.michael.journaler.model.entity.Note
import com.flora.michael.journaler.model.entity.Todo
import retrofit2.Call
import retrofit2.http.*

interface JournalerBackendService {

    companion object {
        fun obtain(): JournalerBackendService = BackendServiceRetrofit.obtain().create(JournalerBackendService::class.java)
    }

    @POST("authenticate")
    fun login(@HeaderMap headers: Map<String, String>, @Body payload: UserLoginRequest): Call<JournalerApiToken>

    @GET("notes")
    fun getNotes(@HeaderMap headers: Map<String, String>): Call<List<Note>>

    @GET("todos")
    fun getTodods(@HeaderMap headers: Map<String, String>): Call<List<Todo>>

    @PUT("notes")
    fun publishNotes(@HeaderMap headers: Map<String, String>, @Body payload: List<Note>): Call<Unit>

    @PUT("todos")
    fun publishTodos(@HeaderMap headers: Map<String,String>, @Body payload: List<Todo>): Call<Unit>

    @DELETE("notes")
    fun removeNotes(@HeaderMap headers: Map<String, String>, @Body payload: List<Note>): Call<Unit>

    @DELETE("todos")
    fun removeTodos(@HeaderMap headers: Map<String, String>, @Body payload: List<Todo>): Call<Unit>

}