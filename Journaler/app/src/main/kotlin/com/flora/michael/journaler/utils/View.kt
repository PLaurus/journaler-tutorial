package com.flora.michael.journaler.utils

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup

fun View.setMargins(left: Int? = null, right: Int? = null, bottom: Int? = null, top: Int? = null){
    if(layoutParams is ViewGroup.MarginLayoutParams){
        val params = layoutParams as ViewGroup.MarginLayoutParams
        left?.run { params.leftMargin = this.dpToPx(context) }
        top?.run { params.topMargin = this.dpToPx(context) }
        right?.run { params.rightMargin = this.dpToPx(context) }
        bottom?.run { params.bottomMargin = this.dpToPx(context) }
        requestLayout()
    }
}

fun Int.dpToPx(context: Context): Int {
    val metrics = context.resources.displayMetrics
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), metrics).toInt()
}