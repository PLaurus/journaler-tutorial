package com.flora.michael.journaler.ui.todoseditor

import android.location.Location
import android.util.Log
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.flora.michael.journaler.Journaler
import com.flora.michael.journaler.model.entity.Todo
import com.flora.michael.journaler.model.repository.TodosRepository
import com.flora.michael.journaler.utils.isSet
import com.flora.michael.journaler.utils.setTime
import kotlinx.coroutines.launch
import java.util.*

class TodoEditorViewModel : ViewModel(){
    private val tag = TodoEditorViewModel::class.java.simpleName
    private val todoRepository = TodosRepository(Journaler.database.todoDao)

    private val dateTimeFrom = MutableLiveData<Calendar>().apply { value = Calendar.getInstance().apply { clear() } }
    private var dateTimeTo = MutableLiveData<Calendar>().apply { value = Calendar.getInstance().apply { clear() } }
    private val isDateToSet
        get() = dateTimeTo.value?.isSet

    var title = MutableLiveData<String>().apply{ value = "" }
    var content = MutableLiveData<String>().apply { value = "" }
    var location = Location("")

    val error : LiveData<Error?> = MediatorLiveData<Error?>().apply {
        val onChanged = Observer<Any?> {
            val title = title.value
            val dateTimeFrom = dateTimeFrom.value

            Log.v(tag, "Checking todo input fields...")
            value = when{
                title.isNullOrBlank() -> Error.EMPTY_TODO_TITLE
                dateTimeFrom?.isSet(Calendar.YEAR)?.and(dateTimeFrom.isSet(Calendar.YEAR))?.and(dateTimeFrom.isSet(Calendar.YEAR)) == false -> Error.FROM_DATE_NOT_SET
                isDateToSet == true && dateTimeTo.value?.before(dateTimeFrom) == true -> Error.TO_DATE_IS_LESS_THEN_FROM_DATE
                else -> null
            }

            Log.i(tag, value.toString())
        }

        addSource(title, onChanged)
        addSource(dateTimeFrom, onChanged)
        addSource(dateTimeTo, onChanged)
    }

    fun initialize(){
        title.value = ""
        content.value = ""
        location = Location("")
        dateTimeFrom.value = Calendar.getInstance().apply { clear() }
        dateTimeTo.value = Calendar.getInstance().apply { clear() }
    }

    fun setDateFrom(year: Int, month: Int, day: Int) {
        dateTimeFrom.value = dateTimeFrom.value?.apply { set(year, month, day) }
    }

    fun setTimeFrom(hours: Int, minutes: Int, seconds: Int = 0) {
        dateTimeFrom.value = dateTimeFrom.value?.apply{ setTime(hours, minutes, seconds) }
    }

    fun setDateTo(year: Int, month: Int, day: Int) {
        dateTimeTo.value = dateTimeTo.value?.apply { set(year, month, day) }
    }

    fun setTimeTo(hours: Int, minutes: Int, seconds: Int = 0) {
        dateTimeTo.value = dateTimeTo.value?.apply{ setTime(hours, minutes, seconds) }
    }

    fun saveNewTodo(): Boolean{
        if(error.value != null)
            return false

        val dateFromInMillis = dateTimeFrom.value?.timeInMillis ?: 0
        val dateToInMillis = if(isDateToSet == true) dateTimeTo.value?.timeInMillis ?: dateFromInMillis else dateFromInMillis

        val todo = Todo(title.value ?: "", content.value ?: "", Location(""), dateFromInMillis, dateToInMillis)

        viewModelScope.launch {
            todoRepository.insert(todo)
        }

        return true
    }

    enum class Error{
        EMPTY_TODO_TITLE,
        FROM_DATE_NOT_SET,
        TO_DATE_IS_LESS_THEN_FROM_DATE
    }
}