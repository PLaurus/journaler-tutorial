package com.flora.michael.journaler.database

//import android.content.ContentValues
//import android.database.Cursor
//import android.location.Location
//import android.util.Log
//import com.flora.michael.journaler.model.entity.abstracted.DbModel
//import com.flora.michael.journaler.model.entity.Note
//import com.flora.michael.journaler.model.entity.Todo
//import com.github.salomonbrys.kotson.fromJson
//import com.google.gson.Gson
//
//
//object Db {
//    const val version = 1
//    const val name = "journaler"
//    private const val tag = "Db"
//    private val dbHelper = DbHelper(name, version)
//    private val gson = Gson()
//
//    val NOTE = object : Crud<Note>{
//        val table = DbHelper.TABLE_NOTES
//
//        override fun insert(what: Note): Long {
//            val inserted = insert(listOf(what))
//
//            if(inserted.isNotEmpty()) return inserted[0]
//
//            return 0
//        }
//
//        override fun insert(what: Collection<Note>): List<Long> {
//            val values = mutableListOf<ContentValues>()
//            what.forEach{note ->
//                values.add(ContentValues().apply{
//                    put(DbHelper.COLUMN_TITLE, note.title)
//                    put(DbHelper.COLUMN_MESSAGE, note.message)
//                    put(DbHelper.COLUMN_LOCATION, gson.toJson(note.location))
//                })
//            }
//
//            return insert(table, values){ id, row ->
//                Log.v(tag, """Inserted note "${row.getAsString(DbHelper.COLUMN_TITLE)}". ID: [ $id ] """)
//            }
//        }
//
//        override fun update(what: Note): Int = update(listOf(what))
//
//        override fun update(what: Collection<Note>): Int {
//            val rowsById = mutableMapOf<Long, ContentValues>()
//            what.forEach{note ->
//                rowsById[note.id] = ContentValues().apply{
//                    put(DbHelper.COLUMN_TITLE, note.title)
//                    put(DbHelper.COLUMN_MESSAGE, note.message)
//                    put(DbHelper.COLUMN_LOCATION, gson.toJson(note.location))
//                }
//            }
//
//            return update(table, rowsById)
//        }
//
//        override fun delete(what: Note): Int = delete(listOf(what))
//
//        override fun delete(what: Collection<Note>): Int = delete(table, what)
//
//        override fun select(args: Pair<String, String>): List<Note> = select(listOf(args))
//
//        override fun select(args: Collection<Pair<String, String>>): List<Note> {
//            return select(table, args, cursorToModelPredicate) as List<Note>
//        }
//
//        override fun selectAll(): List<Note> = select(table = table, toModel = cursorToModelPredicate) as List<Note>
//
//        val cursorToModelPredicate: Cursor.() -> DbModel = {
//            val title = getString(getColumnIndexOrThrow(DbHelper.COLUMN_TITLE))
//            val message = getString(getColumnIndexOrThrow(DbHelper.COLUMN_MESSAGE))
//            val location = gson.fromJson<Location>(getString(getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION)))
//
//            Note(title, message, location)
//        }
//    }
//
//    val TODO = object : Crud<Todo>{
//        val table = DbHelper.TABLE_TODOS
//
//        override fun insert(what: Todo): Long {
//            val inserted = insert(listOf(what))
//
//            if(inserted.isNotEmpty()) return inserted[0]
//
//            return 0
//        }
//
//        override fun insert(what: Collection<Todo>): List<Long> {
//            val values = mutableListOf<ContentValues>()
//            what.forEach{todo ->
//                values.add(ContentValues().apply{
//                    put(DbHelper.COLUMN_TITLE, todo.title)
//                    put(DbHelper.COLUMN_MESSAGE, todo.message)
//                    put(DbHelper.COLUMN_FROM_TIME, todo.fromDate)
//                    put(DbHelper.COLUMN_TO_TIME, todo.toDate)
//                    put(DbHelper.COLUMN_LOCATION, gson.toJson(todo.location))
//                })
//            }
//
//            return insert(table, values){ id, row ->
//                Log.v(tag, """Inserted todo "${row.getAsString(DbHelper.COLUMN_TITLE)}". ID: [ $id ] """)
//            }
//        }
//
//        override fun update(what: Todo): Int = update(listOf(what))
//
//        override fun update(what: Collection<Todo>): Int {
//            val rowsById = mutableMapOf<Long, ContentValues>()
//            what.forEach{todo ->
//                rowsById[todo.id] = ContentValues().apply{
//                    put(DbHelper.COLUMN_TITLE, todo.title)
//                    put(DbHelper.COLUMN_MESSAGE, todo.message)
//                    put(DbHelper.COLUMN_FROM_TIME, todo.fromDate)
//                    put(DbHelper.COLUMN_TO_TIME, todo.toDate)
//                    put(DbHelper.COLUMN_LOCATION, gson.toJson(todo.location))
//                }
//            }
//
//            return update(table, rowsById)
//        }
//
//        override fun delete(what: Todo): Int = delete(listOf(what))
//
//        override fun delete(what: Collection<Todo>): Int = delete(table, what)
//
//        override fun select(args: Pair<String, String>): List<Todo> = select(listOf(args))
//
//        override fun select(args: Collection<Pair<String, String>>): List<Todo> {
//            return select(table, args, cursorToModelPredicate) as List<Todo>
//        }
//
//        override fun selectAll(): List<Todo> = select(table = table, toModel = cursorToModelPredicate) as List<Todo>
//
//        val cursorToModelPredicate: Cursor.() -> DbModel = {
//            val title = getString(getColumnIndexOrThrow(DbHelper.COLUMN_TITLE))
//            val message = getString(getColumnIndexOrThrow(DbHelper.COLUMN_MESSAGE))
//            val location = gson.fromJson<Location>(getString(getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION)))
//            val fromDate = getLong(getColumnIndexOrThrow(DbHelper.COLUMN_FROM_TIME))
//            val toDate = getLong(getColumnIndexOrThrow(DbHelper.COLUMN_TO_TIME))
//
//            Todo(
//                title,
//                message,
//                location,
//                fromDate,
//                toDate
//            )
//        }
//
//
//    }
//
//    private fun insert(table: String, rows: Collection<ContentValues>, onRowInserted: (id: Long, row: ContentValues) -> Unit = {_, _ -> Unit}) : List<Long> {
//        val db = dbHelper.writableDatabase
//        db.beginTransaction()
//
//        val ids = mutableListOf<Long>()
//        rows.forEach{ value ->
//            val id = db.insert(table, null, value)
//
//            if(id > 0){
//                ids.add(id)
//                onRowInserted(id, value)
//            }
//        }
//
//        if(ids.size == rows.size){
//            db.setTransactionSuccessful()
//        }else{
//            ids.clear()
//        }
//
//        with(db){
//            endTransaction()
//            close()
//        }
//
//        return ids
//    }
//
//    private fun update(table: String, rowsById: Map<Long,ContentValues>) : Int{
//        val db = dbHelper.writableDatabase
//        db.beginTransaction()
//        var updated = 0
//
//        rowsById.forEach{ entry ->
//            db.update(table, entry.value, "_id = ?", arrayOf(entry.key.toString()))
//            updated++
//        }
//
//        if(updated == rowsById.size){
//            db.setTransactionSuccessful()
//        }else{
//            updated = 0
//        }
//
//        with(db){
//            endTransaction()
//            close()
//        }
//
//        return updated
//    }
//
//    private fun delete(table: String, rows: Collection<DbModel>): Int{
//        val db = dbHelper.writableDatabase
//        db.beginTransaction()
//
//        val ids = StringBuilder()
//        rows.forEachIndexed{ index, item ->
//            ids.append(item.id.toString())
//            if(index < rows.size - 1) ids.append(", ")
//        }
//
//        val statement = db.compileStatement("DELETE FROM $table WHERE $DbHelper.ID IN $ids;")
//        val countOfDeleted = statement.executeUpdateDelete()
//
//        if(countOfDeleted > 0){
//            db.setTransactionSuccessful()
//            Log.i(tag, "Successfully deleted $countOfDeleted [ $statement ]")
//        }else{
//            Log.w(tag, "Deletion failed [ $statement ]" )
//        }
//
//        with(db){
//            endTransaction()
//            close()
//        }
//
//        return countOfDeleted
//    }
//
//    private fun select(table: String, whereArgs: Collection<Pair<String, String>> = emptyList(), toModel: Cursor.() -> DbModel): List<DbModel>{
//        val db = dbHelper.writableDatabase
//        val selection = StringBuilder()
//        val selectionArgs = mutableListOf<String>()
//
//        val cursor = if(whereArgs.isNotEmpty()) {
//            whereArgs.forEach { arg ->
//                selection.append("${arg.first} == ?")
//                selectionArgs.add(arg.second)
//            }
//
//
//            db.query(
//                true,
//                table,
//                null,
//                selection.toString(),
//                selectionArgs.toTypedArray(),
//                null, null, null, null
//            )
//        }else{
//            db.query(
//                true,
//                table,
//                null,
//                null,
//                null,
//                null, null, null, null
//            )
//        }
//
//        val result = mutableListOf<DbModel>()
//
//        while(cursor.moveToNext()){
//            val tableEntry: DbModel = cursor.toModel()
//            val id = cursor.getLong(cursor.getColumnIndexOrThrow(DbHelper.ID))
//            tableEntry.id = id
//            result.add(tableEntry)
//        }
//
//        cursor.close()
//        return result
//    }
//
//}