package com.flora.michael.journaler.model

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.flora.michael.journaler.model.dao.NoteDao
import com.flora.michael.journaler.model.dao.TodoDao
import com.flora.michael.journaler.model.entity.Note
import com.flora.michael.journaler.model.entity.Todo

@Database(
    entities = [Note::class, Todo::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase(){
    abstract val noteDao: NoteDao
    abstract val todoDao: TodoDao
}