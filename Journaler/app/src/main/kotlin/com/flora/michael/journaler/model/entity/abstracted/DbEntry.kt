package com.flora.michael.journaler.model.entity.abstracted

import android.location.Location
import androidx.room.ColumnInfo

abstract class DbEntry(
    @ColumnInfo(name = "title")
    var title: String,

    @ColumnInfo(name = "message")
    var message: String,

    @ColumnInfo(name = "location")
    var location: Location

) : DbModel()