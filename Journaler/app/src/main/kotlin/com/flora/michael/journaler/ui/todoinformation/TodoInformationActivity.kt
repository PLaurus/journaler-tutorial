package com.flora.michael.journaler.ui.todoinformation

import com.flora.michael.journaler.ui.abstracted.activity.BaseActivity

class TodoInformationActivity : BaseActivity() {
    override val tag: String = "Todo information activity"

    override fun getLayout(): Int = TODO()

    override fun getActivityTitle(): Int = TODO()
}