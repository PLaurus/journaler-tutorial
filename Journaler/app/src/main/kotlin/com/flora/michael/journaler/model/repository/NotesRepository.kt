package com.flora.michael.journaler.model.repository

import android.util.Log
import com.flora.michael.journaler.model.dao.NoteDao
import com.flora.michael.journaler.model.entity.Note

class NotesRepository(private val notesDao: NoteDao) {
    private val tag = NotesRepository::class.java.simpleName

    fun getAll() = notesDao.getAllLive()

    suspend fun insert(note: Note) {
        val insertedId = notesDao.insert(note)

        var message = "Failed to insert the note"
        if(insertedId > 0) message = "Successful note insertion"

        Log.i(tag, message)
    }

    suspend fun update(note: Note) {
        val updatedCount = notesDao.update(note)

        var message = "Failed to update the note"
        if(updatedCount > 0) message = "Note successfully updated"

        Log.i(tag, message)
    }

    suspend fun delete(note: Note): Int {
        val deletedCount = notesDao.delete(note)

        var message = "Failed to delete the note"
        if(deletedCount > 0) message = "Note successfully deleted"

        Log.i(tag, message)

        return deletedCount
    }
}