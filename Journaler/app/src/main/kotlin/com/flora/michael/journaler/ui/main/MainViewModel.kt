package com.flora.michael.journaler.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.flora.michael.journaler.Journaler
import com.flora.michael.journaler.model.entity.Note
import com.flora.michael.journaler.model.entity.Todo
import com.flora.michael.journaler.model.repository.NotesRepository
import com.flora.michael.journaler.model.repository.TodosRepository

class MainViewModel : ViewModel(){
    private val tag = "Main_view_model"
    private val database = Journaler.database
    private val notesRepository = NotesRepository(database.noteDao)
    private val todosRepository = TodosRepository(database.todoDao)
    private val notesSizeMutableLiveData = MutableLiveData<Int>().apply { value = 0 }
    private val todosSizeMutableLiveData = MutableLiveData<Int>().apply { value = 0 }

    val allNotes = notesRepository.getAll()
    val notesSize: LiveData<Int> = notesSizeMutableLiveData

    val allTodos = todosRepository.getAllLive()
    val todosSize: LiveData<Int> = notesSizeMutableLiveData

    init{
        allNotes.observeForever {
            notesSizeMutableLiveData.value = it.size
        }
        allTodos.observeForever {
            todosSizeMutableLiveData.value = it.size
        }
    }

    suspend fun delete(note: Note) = notesRepository.delete(note)

    suspend fun delete(todo: Todo) = todosRepository.delete(todo)

}