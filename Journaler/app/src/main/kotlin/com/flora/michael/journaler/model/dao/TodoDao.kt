package com.flora.michael.journaler.model.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.flora.michael.journaler.model.entity.Todo

@Dao
interface TodoDao {
    @Query("SELECT * FROM todos")
    fun getAllLive(): LiveData<List<Todo>>

    @Query("SELECT * FROM todos")
    suspend fun selectAll(): List<Todo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(todo: Todo): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(todos: List<Todo>): List<Long>

    @Update
    suspend fun update(todo: Todo): Int

    @Update
    suspend fun update(todos: List<Todo>): Int

    @Delete
    suspend fun delete(todo: Todo): Int

    @Delete
    suspend fun delete(todos: List<Todo>): Int
}