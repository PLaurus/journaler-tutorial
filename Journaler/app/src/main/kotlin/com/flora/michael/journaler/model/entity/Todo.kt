package com.flora.michael.journaler.model.entity

import android.location.Location
import android.location.LocationManager
import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import com.flora.michael.journaler.model.entity.abstracted.DbEntry

@Entity(tableName = "todos")
class Todo(
    title:String,
    message: String,
    location: Location,

    @ColumnInfo(name = "from_date")
    val fromDate: Long,

    @ColumnInfo(name = "to_date")
    val toDate: Long
) : DbEntry(title, message, location), Parcelable{

    constructor(parcel: Parcel?) :
            this(
                parcel?.readString() ?: "undefined",
                parcel?.readString() ?: "undefined",
                parcel?.readParcelable(Location::class.java.classLoader) ?: Location(LocationManager.GPS_PROVIDER),
                parcel?.readLong() ?: 0,
                parcel?.readLong() ?: 0){

        id = parcel?.readLong() ?: 0L
    }

    override fun writeToParcel(parcel: Parcel?, flags: Int) {
        parcel?.let{
            with(parcel){
                writeString(title)
                writeString(message)
                writeParcelable(location, 0)
                writeLong(fromDate)
                writeLong(toDate)
                writeLong(id)
            }
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Todo> {
        override fun createFromParcel(parcel: Parcel): Todo {
            return Todo(parcel)
        }

        override fun newArray(size: Int): Array<Todo?> {
            return arrayOfNulls(size)
        }
    }
}