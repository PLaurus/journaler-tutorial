package com.flora.michael.journaler.ui.main

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.flora.michael.journaler.R
import com.flora.michael.journaler.preferences.PreferencesConfiguration
import com.flora.michael.journaler.preferences.PreferencesProvider
import com.flora.michael.journaler.service.MainService
import com.flora.michael.journaler.ui.abstracted.activity.BaseActivity
import com.flora.michael.journaler.ui.main.notes.NotesListFragment
import com.flora.michael.journaler.ui.main.todos.TodosListFragment
import com.flora.michael.journaler.ui.noteseditor.NoteEditorActivity
import com.flora.michael.journaler.ui.todoseditor.TodoEditorActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    override val tag = "Main activity"
    override fun getLayout() = R.layout.activity_main
    override fun getActivityTitle(): Int = R.string.app_name

    companion object {
        private const val PAGE_POSITION_KEY = "PAGE_POSITION_KEY"
    }

    private var service: MainService? = null

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName?, binder: IBinder?) {
            if (binder is MainService.MainServiceBinder) {
                service = binder.getService()
                service?.let {
                    left_drawer.menu.findItem(R.id.synchronize_button).isEnabled = true
                }
            }
        }

        override fun onServiceDisconnected(componentName: ComponentName?) {
            service = null
            left_drawer.menu.findItem(R.id.synchronize_button).isEnabled = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        new_item_button?.initializeCreateButton()

        val provider = PreferencesProvider()
        val config = PreferencesConfiguration("journaler_prefs", Context.MODE_PRIVATE)
        val preferences = provider.obtain(config, this)

        pager.adapter = ViewPagerAdapter(supportFragmentManager)
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) = Unit

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) = Unit

            override fun onPageSelected(position: Int) {
                Log.v(tag, "Page [ $position ]")
                preferences.edit().putInt(PAGE_POSITION_KEY, position).apply()
            }
        })

        val pagerPosition = preferences.getInt(PAGE_POSITION_KEY, 0)
        pager.setCurrentItem(pagerPosition, true)

        initializeBottomNavigation(pager)

        initializeNavigationView(left_drawer)

        val serviceIntent = Intent(this, MainService::class.java)
        startService(serviceIntent)
    }

    override fun onResume() {
        super.onResume()
        val intent = Intent(this, MainService::class.java)
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onPause() {
        super.onPause()
        unbindService(serviceConnection)
    }

    private fun initializeNavigationView(navigationView: NavigationView?) {
        navigationView?.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.show_home_button -> {
                    pager.setCurrentItem(0, true)
                    true
                }
                R.id.show_notes_button -> {
                    pager.setCurrentItem(1, true)
                    true
                }
                R.id.show_todos_button -> {
                    pager.setCurrentItem(2, true)
                    true
                }
                R.id.synchronize_button -> {
                    if (menuItem.isEnabled) {
                        service?.synchronize()
                        true
                    } else {
                        false
                    }
                }
                else -> false
            }
        }
    }

    private fun FloatingActionButton.initializeCreateButton() {
        setOnClickListener {
            //bounceAnimation()
            val items = arrayOf(getString(R.string.todos), getString(R.string.notes))
            val builder = AlertDialog.Builder(context ?: throw IllegalArgumentException("Alert dialog builder must get context!"))
                .setTitle(R.string.choose_a_type)
                .setCancelable(true)
                .setOnCancelListener { /*bounceAnimation(false)*/ }
                .setItems(items) { _, option ->
                    when (option) {
                        0 -> startCreateTodoActivity()
                        1 -> startCreateNoteActivity()
                        else -> Log.e(
                            this@MainActivity.tag,
                            "Unknown option selected [ $option ]"
                        )
                    }
                }

            builder.show()
        }
    }

    private fun startCreateTodoActivity() {
        startActivity(Intent(this, TodoEditorActivity::class.java))
    }

    private fun startCreateNoteActivity() {
        startActivity(Intent(this, NoteEditorActivity::class.java))
    }

    private fun initializeBottomNavigation(viewPager: ViewPager) {
        bottom_pager_navigation.setupWithViewPager(viewPager, false)
        for (page in PAGE.values()) {
            bottom_pager_navigation.getTabAt(page.position)?.setIcon(page.iconRes)
        }
    }

    private enum class PAGE(val position: Int, public val titleRes: Int, val iconRes: Int) {
        HOME(0, R.string.home_bottom_menu, R.drawable.ic_home),
        NOTES(1, R.string.notes_bottom_menu, R.drawable.ic_notes),
        TODOS(2, R.string.todos_bottom_menu, R.drawable.ic_done_all);

        companion object {
            fun getByPosition(pos: Int) = values().find { it.position == pos } ?: HOME
        }
    }

    private inner class ViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getPageTitle(position: Int): CharSequence? = this@MainActivity.resources.getString(PAGE.getByPosition(position).titleRes)

        val homeFragment = Fragment()
        val notesListFragment = NotesListFragment()
        val todosListFragment = TodosListFragment()

        override fun getItem(position: Int): Fragment =
            when (PAGE.getByPosition(position)) {
                PAGE.HOME -> homeFragment
                PAGE.NOTES -> notesListFragment
                PAGE.TODOS -> todosListFragment
            }

        override fun getCount(): Int = 3
    }
}

