package com.flora.michael.journaler.model.entity.abstracted

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

abstract class DbModel{
    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}