package com.flora.michael.journaler.model.repository

import android.util.Log
import com.flora.michael.journaler.model.dao.TodoDao
import com.flora.michael.journaler.model.entity.Todo

class TodosRepository(private val todosDao: TodoDao) {
    private val tag = TodosRepository::class.java.simpleName

    fun getAllLive() = todosDao.getAllLive()

    suspend fun insert(todo: Todo) {
        val insertedId = todosDao.insert(todo)

        var message = "Failed to insert the todo"
        if(insertedId > 0) message = "Successful todo insertion"

        Log.i(tag, message)
    }

    suspend fun update(todo: Todo) {
        val updatedCount = todosDao.update(todo)

        var message = "Failed to update the todo"
        if(updatedCount > 0) message = "Todo successfully updated"

        Log.i(tag, message)
    }

    suspend fun delete(todo: Todo): Int{
        val deletedCount = todosDao.delete(todo)

        var message = "Failed to delete the todo"
        if(deletedCount > 0) message = "Todo successfully deleted"

        Log.i(tag, message)

        return deletedCount
    }
}