package com.flora.michael.journaler.ui.noteseditor

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.flora.michael.journaler.R
import com.flora.michael.journaler.ui.abstracted.activity.ItemActivity
import com.flora.michael.journaler.utils.StyledSnackBar
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_note.*
import kotlinx.android.synthetic.main.save_cancel_buttons.*

class NoteEditorActivity : ItemActivity() {
    override val tag: String = "Note activity"
    override fun getLayout() = R.layout.activity_note

    private val viewModel: NoteEditorViewModel by lazy{
        ViewModelProviders.of(this)[NoteEditorViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.error.observe(this, Observer{})

        viewModel.initialize()

        note_title.addTextChangedListener { editable ->
            viewModel.title.value = editable?.toString() ?: ""
        }

        note_content.addTextChangedListener{ editable ->
            viewModel.content.value = editable?.toString() ?: ""
        }

        save_button.initializeSaveButton()
    }

    private fun MaterialButton.initializeSaveButton(){
        setOnClickListener (object : View.OnClickListener{
            override fun onClick(view: View?) {

                val message = when(viewModel.error.value){
                    NoteEditorViewModel.Error.EMPTY_NOTE_TITLE -> "Note title must not be empty!"
                    else -> null
                }

                message?.let{
                    StyledSnackBar.make(this@NoteEditorActivity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show()
                    return
                }

                viewModel.saveNewNote()

                finish()
            }
        })
    }




}