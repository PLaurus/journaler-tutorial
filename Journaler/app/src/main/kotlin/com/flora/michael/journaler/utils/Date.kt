package com.flora.michael.journaler.utils

import java.util.*

fun Calendar.setTime(hours: Int, minutes: Int, seconds: Int = 0){
    set(Calendar.HOUR_OF_DAY, hours)
    set(Calendar.MINUTE, minutes)
    set(Calendar.SECOND, seconds)
}

val Calendar.isSet: Boolean
    get() {
        val isYearSet = this.isSet(Calendar.YEAR)
        val isMonthSet = this.isSet(Calendar.MONTH)
        val isDaySet = this.isSet(Calendar.DAY_OF_MONTH)
        val isHourSet = this.isSet(Calendar.HOUR_OF_DAY)
        val isMinuteSet = this.isSet(Calendar.MINUTE)
        val isSecondSet = this.isSet(Calendar.SECOND)
        val isMillisecondSet = this.isSet(Calendar.MILLISECOND)

        return isYearSet || isMonthSet || isDaySet || isHourSet || isMinuteSet || isSecondSet || isMillisecondSet
    }

fun Long.millisToMinutes() = (this / 1000) / 60
fun Long.millisToDays() : Long = (millisToMinutes() / 60) / 24