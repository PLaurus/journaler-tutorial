package com.flora.michael.journaler.model.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.flora.michael.journaler.model.entity.Note

@Dao
interface NoteDao {
    @Query("SELECT * FROM notes")
    fun getAllLive(): LiveData<List<Note>>

    @Query("SELECT * FROM notes")
    suspend fun selectAll(): List<Note>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(note: Note): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(notes: List<Note>): List<Long>

    @Update
    suspend fun update(note: Note): Int

    @Update
    suspend fun update(notes: List<Note>): Int

    @Delete
    suspend fun delete(note: Note): Int

    @Delete
    suspend fun delete(notes: List<Note>): Int

}